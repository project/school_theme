(function ($, Drupal, window, document, undefined) {
    Drupal.behaviors.custom_slider_gl_js = {
      attach: function (context, settings) {
        
        if(jQuery(".testimonials-list").length) {
            jQuery(".testimonials-list").not('.slick-initialized').slick({
              infinite: false,
              speed: 800,
              slidesToShow: 3,
              slidesToScroll: 1,
              responsive: [
                {
                  breakpoint: 1100,
                  settings: {
                    slidesToShow: 3.2,
                  },
                },
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 2.2,
                  },
                },
                {
                  breakpoint: 767,
                  settings: "unslick"
                }
              ],
            });
          }
        

        }

    }

  //Read More for All with maxHeight condition.
  jQuery(window).on('load', function () {
    if(jQuery('.facilities-bottom > p').length) {
      jQuery('.facilities-bottom > p').showMore({
        speedDown: 300,
        speedUp: 300,
        height: 39,
        showText: '+ More',
        hideText: '- Less'
      });
    }
  });

  //Faq Toggle Start 
  jQuery('.faq-sec .faq-title, .view-details-click').click(function () {
    jQuery(this).toggleClass('active')
    jQuery(this).parent().siblings().children().removeClass('active')
    jQuery(this).next().slideToggle()
    jQuery(this).parent().siblings().children().next().slideUp();
    return false;
  });
  //Faq Toggle End

  //Faq page ID Start
  if(window.location.hash) {
    var hash_id = window.location.hash.split('#')[ 1 ];
    jQuery('#' + hash_id).addClass('scroll-margin-top active').next().show();
  }
  //Faq page ID End
  
})(jQuery, Drupal, this, this.document);

jQuery(document).ready(function(){
  
  jQuery('.flexslider').flexslider({
    animation: "slides"
    
  }); 

  })